// Importa las funciones que necesitas del SDK
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.12.2/firebase-app.js";
import { getDatabase, ref as refS, set, child, get, update, remove, onValue } 
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-database.js";
import { getStorage, ref, uploadBytesResumable, getDownloadURL }
from "https://www.gstatic.com/firebasejs/10.12.2/firebase-storage.js";

const firebaseConfig = {
    apiKey: "AIzaSyC2THGtWUos9hUT_XEo0vjgQ7aWLNb_nbs",
    authDomain: "adminpastel-1f4b9.firebaseapp.com",
    databaseURL: "https://adminpastel-1f4b9-default-rtdb.firebaseio.com",
    projectId: "adminpastel-1f4b9",
    storageBucket: "adminpastel-1f4b9.appspot.com",
    messagingSenderId: "550473494532",
    appId: "1:550473494532:web:d1357be374ef09d9214551"
};

// Inicializa Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase(app);
const storage = getStorage();

const imageInput = document.getElementById('imageInput');
const uploadButton = document.getElementById('uploadButton');
const progressDiv = document.getElementById('progress');
const txtUrlInput = document.getElementById('txtUrl');

// Declarar variables globales
var codigo = "";
var tematica = "";
var objetos = "";
var descripcion = "";
var urlImag = "";

function leerInputs() {
    codigo = document.getElementById('txtCodigo').value;
    tematica = document.getElementById('txtTematica').value;
    objetos = document.getElementById('txtObjetos').value;
    descripcion = document.getElementById('txtDescripcion').value;
    urlImag = document.getElementById('txtUrl').value;
}

function mostrarMensaje(mensaje) {
    var mensajeElement = document.getElementById('mensaje');
    mensajeElement.textContent = mensaje;
    mensajeElement.style.display = 'block';
    setTimeout(() => {
        mensajeElement.style.display = 'none';
    }, 1000);
}

function insertarProducto() {
    alert("ingrese a add db");
    leerInputs();

    if (codigo === "" || tematica === "" || objetos === "" || descripcion === "") {
        mostrarMensaje("faltaron datos por capturar");
        return;
    }

    set(refS(db, 'Pasteles/' + codigo), {
        codigo: codigo,
        tematica: tematica,
        objetos: objetos,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        alert("se agregó con éxito");
        Listarproductos();
    }).catch((error) => {
        alert("ocurrió un error");
    });
}

function limpiarInputs() {
    document.getElementById('txtCodigo').value = '';
    document.getElementById('txtTematica').value = '';
    document.getElementById('txtObjetos').value = '';
    document.getElementById('txtDescripcion').value = '';
    document.getElementById('txtUrl').value = '';
}

function escribirInputs() {
    document.getElementById('txtCodigo').value = codigo;
    document.getElementById('txtTematica').value = tematica;
    document.getElementById('txtObjetos').value = objetos;
    document.getElementById('txtDescripcion').value = descripcion;
    document.getElementById('txtUrl').value = urlImag;
}

function buscarProducto() {
    let codigo = document.getElementById('txtCodigo').value.trim();
    if (codigo === "") {
        mostrarMensaje("no se ingresó código");
        return;
    }
    const dbref = refS(db);
    get(child(dbref, 'Pasteles/' + codigo)).then((snapshot) => {
        if (snapshot.exists()) {
            tematica = snapshot.val().tematica;
            objetos = snapshot.val().objetos;
            descripcion = snapshot.val().descripcion;
            urlImag = snapshot.val().urlImag;
            escribirInputs();
        } else {
            limpiarInputs();
            mostrarMensaje("El producto con código " + codigo + " no existe.");
        }
    });
}

function Listarproductos() {
    const dbRef = refS(db, 'Pasteles');
    const tabla = document.getElementById('tablaProductos');
    const tbody = tabla.querySelector('tbody');
    tbody.innerHTML = '';

    onValue(dbRef, (snapshot) => {
        snapshot.forEach((childSnapshot) => {
            const data = childSnapshot.val();
            var fila = document.createElement('tr');

            var celdaCodigo = document.createElement('td');
            celdaCodigo.textContent = data.codigo;
            fila.appendChild(celdaCodigo);

            var celdaTematica = document.createElement('td');
            celdaTematica.textContent = data.tematica;
            fila.appendChild(celdaTematica);

            var celdaObjetos = document.createElement('td');
            celdaObjetos.textContent = data.objetos;
            fila.appendChild(celdaObjetos);

            var celdaDescripcion = document.createElement('td');
            celdaDescripcion.textContent = data.descripcion;
            fila.appendChild(celdaDescripcion);

            var celdaImagen = document.createElement('td');
            var imagen = document.createElement('img');
            imagen.src = data.urlImag;
            imagen.width = 100;
            celdaImagen.appendChild(imagen);
            fila.appendChild(celdaImagen);
            tbody.appendChild(fila);
        });
    });
}

function actualizarProducto() {
    leerInputs();
    if (codigo === "" || tematica === "" || objetos === "" || descripcion === "") {
        mostrarMensaje("favor de capturar toda la información.");
        return;
    }

    update(refS(db, 'Pasteles/' + codigo), {
        codigo: codigo,
        tematica: tematica,
        objetos: objetos,
        descripcion: descripcion,
        urlImag: urlImag
    }).then(() => {
        mostrarMensaje("se actualizó con éxito.");
        limpiarInputs();
        Listarproductos();
    }).catch((error) => {
        mostrarMensaje("ocurrió un error: " + error);
    });
}

function eliminarProducto() {
    let codigo = document.getElementById('txtCodigo').value.trim();
    if (codigo === "") {
        mostrarMensaje("no se ingresó código válido.");
        return;
    }
    const dbref = refS(db);
    get(child(dbref, 'Pasteles/' + codigo)).then((snapshot) => {
        if (snapshot.exists()) {
            remove(refS(db, 'Pasteles/' + codigo))
                .then(() => {
                    mostrarMensaje("producto eliminado con éxito.");
                    limpiarInputs();
                    Listarproductos();
                })
                .catch((error) => {
                    mostrarMensaje("ocurrió un error al eliminar el producto: " + error);
                });
        } else {
            limpiarInputs();
            mostrarMensaje("El Producto con código " + codigo + " no existe.");
        }
    });
}

const btnAgregar = document.getElementById('btnAgregar');
btnAgregar.addEventListener('click', insertarProducto);

const btnBuscar = document.getElementById('btnBuscar');
btnBuscar.addEventListener('click', buscarProducto);

const btnActualizar = document.getElementById('btnActualizar');
btnActualizar.addEventListener('click', actualizarProducto);

const btnBorrar = document.getElementById('btnBorrar');
btnBorrar.addEventListener('click', eliminarProducto);

uploadButton.addEventListener('click', (event) => {
    event.preventDefault();
    const file = imageInput.files[0];

    if (file) {
        const storageRef = ref(storage, file.name);
        const uploadTask = uploadBytesResumable(storageRef, file);
        uploadTask.on('state_changed', (snapshot) => {
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            progressDiv.textContent = 'progreso:' + progress.toFixed(2) + '%';
            
        }, (error) => {
            console.error(error);
        }, () => {
            getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
                txtUrlInput.value = downloadURL;
                setTimeout(() =>{
                    progressDiv.textContent = '';
                }, 500);
            }).catch((error) => {
                console.error(error);
            });
        });

    }
});
